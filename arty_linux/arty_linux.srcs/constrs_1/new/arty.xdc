set_property -dict {PACKAGE_PIN G18 IOSTANDARD LVCMOS33} [get_ports eth_clk]

set_property IOSTANDARD LVCMOS33 [get_ports {ss_o_0[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports io0_i_0]
set_property IOSTANDARD LVCMOS33 [get_ports io0_o_0]
set_property IOSTANDARD LVCMOS33 [get_ports sck_o_0]
set_property PACKAGE_PIN C1 [get_ports {ss_o_0[0]}]
set_property PACKAGE_PIN G1 [get_ports io0_i_0]
set_property PACKAGE_PIN H1 [get_ports io0_o_0]
set_property PACKAGE_PIN F1 [get_ports sck_o_0]

set_property PACKAGE_PIN F1 [get_ports spi_sck_io]
